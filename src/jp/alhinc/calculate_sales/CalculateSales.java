package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL = "売上ファイル名が連番になっていません";
	private static final String FILE_FORMAT_ERROR = "のフォーマットが不正です。";
	private static final String BRANCHCODE_INVALID = "の支店コードが不正です。";
	private static final String SALESAMOUT_DIGIT_OVER = "合計金額が10桁を超えました";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	//String[] argsがコマンドライン引数
	public static void main(String[] args) {

		//コマンドライン引数が1つ設定されているか
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales)) {
			return;
		}

		//.listFilesを使ってパス内のファイルを検索する。
		File[] files = new File(args[0]).listFiles();

		//rcdファイルを保持するためのリストを作成。
		List<File> rcdFiles = new ArrayList<>();
		System.out.println(files);
		for (int i = 0; i < files.length; i++) {

			//売上ファイルに当てはまっているかをチェックする。trueの時に下の命令文実行。
			if (files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				//リストに要素を追加するため。
				rcdFiles.add(files[i]);
			}
		}
		//ファイルの並びを整頓する。
		Collections.sort(rcdFiles);

		//売上ファイルが連番かどうかを確認する
		for (int i = 0; i < rcdFiles.size() - 1; i++) {

			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL);
				return;
			}
		}

		//入っているrcdファイル一つ一つに繰り返し処理を行う
		for (int i = 0; i < rcdFiles.size(); i++) {

			BufferedReader br = null;
			try {

				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;
				List<String> salesFile = new ArrayList<>();

				//一行ずつ読み込む(全ての行を読み込む)
				while ((line = br.readLine()) != null) {
					salesFile.add(line);
				}

				//該当ファイル名の支店コードが不明かどうか
				if (!branchSales.containsKey(salesFile.get(0))) {
					System.out.println(rcdFiles.get(i).getName()  + BRANCHCODE_INVALID);
					return;
				}

				//売上ファイルのフォーマットを確認する。
				if (salesFile.size() != 2) {
					System.out.println(rcdFiles.get(i).getName() + FILE_FORMAT_ERROR);
					return;
				}

				//変数の箱を新しく用意して売上金だけを格納する。
				String sales = salesFile.get(1);

				//売上金額が数字なのかを確認する
				if (!sales.matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//String型をLong型に変換する。
				long fileSales = Long.parseLong(sales);

				//現在の売上金額とファイルの中身を足し合わせる。
				long salesAmount = branchSales.get(salesFile.get(0)) + fileSales;

				//売上金額が10桁を超えたかどうか判定

				if (salesAmount >= 1000000000L) {
					System.out.println(SALESAMOUT_DIGIT_OVER);
					return;
				}
				//salesamountの結果をマップにプットする。
				branchSales.put(salesFile.get(0), salesAmount);

			}catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			}finally {

				// ファイルを開いている場所
				if (br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}
		//支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */

	private static boolean readFile(String path, String fileName, Map<String, String> branchNames,
			Map<String, Long> branchSales) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//ファイルがあるか確認する
			if (!file.exists()) {
				System.out.println(FILE_NOT_EXIST);
				return false;
			}

			//存在するファイルを読み込む
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;

			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {

				//支店コードと店舗名を別々に保持するため
				String[] items = line.split(",");

				//ファイルのフォーマットが正しいかを確認する。
				if ((items.length != 2) || (!items[0].matches("^[0-9]{3}$"))) {
					System.out.println(FILE_INVALID_FORMAT);
					return false;
				}
				branchNames.put(items[0], items[1]);
				branchSales.put(items[0], 0L);
			}
		}catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		}
		finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				}catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */

	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames,Map<String, Long> branchSales) {

		BufferedWriter bw = null;

		//ファイル作成して書き込む処理
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for (String key : branchSales.keySet()) {
				//ファイルに文字を書き込む
				bw.write(key + "," + branchNames.get(key) + "," + branchSales.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		}
		finally {
			// ファイルを開いている場合
			if (bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}